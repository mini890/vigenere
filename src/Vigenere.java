import java.util.Scanner;

public class Vigenere {
	
	private String key;
	
	public Vigenere(String key) {
		setKey(key);
	}
	
	public void setKey(String key) {
		if (key == null) {
			this.key = "";
			return;
		}
		
		char[] digit = key.toUpperCase().toCharArray();
		
		StringBuilder sb = new StringBuilder(digit.length);
		for (char c : digit) {
			if (c >= 'A' && c <= 'Z')
				sb.append(c);
		}
		
		this.key = sb.toString();
	}
	
	public String encode(String clear) {
		if (clear == null)
			return "";
		if (key.length() == 0)
			return clear.toUpperCase();
		
		char[] digit = clear.toLowerCase().toCharArray();
		
		String longKey = key;
		while(longKey.length() < clear.length())
			longKey += key;
		
		for (int i = 0; i < digit.length; i++) {
			
			if (digit[i] < 'a' || digit[i] > 'z')
				continue;
			
		char offset = longKey.charAt(i);
		int nbShift = offset - 'A';
		digit[i] = Character.toUpperCase(digit[i]);
		digit[i] += nbShift;
		if (digit[i] > 'Z') {
			digit[i] -= 'Z';
			digit[i] += ('A' - 1);
		}
		}
		return new String(digit);
	}
	
	public String decode(String coded) {
		if (coded == null)
			return "";
		if(key.length() == 0)
			return coded.toLowerCase();
		
		char[] digit = coded.toUpperCase().toCharArray();
		
		String longKey = key;
		while(longKey.length() < coded.length())
			longKey += key;
		
		for (int i = 0; i < digit.length; i++) {
			if (digit[i] < 'A' || digit[i] > 'Z')
				continue;
			
			char offset = longKey.charAt(i);
			int nbShift = offset - 'A';
			digit[i] = Character.toLowerCase(digit[i]);
			digit[i] -= nbShift;
			
			if (digit[i] < 'a') {
				digit[i] += 'z';
				digit[i] -= ('a' - 1);
			}
		}
		return new String (digit);
	}
	
	public static void main(String[] args) {
		Vigenere vigenere = new Vigenere("DreamInCode");
		String alphabet = "abcdefghijklmnopqrstuvwxyz";
		String coded = vigenere.encode(alphabet);
		System.out.println("Coded alphabet is: " + coded);
		String clear = vigenere.decode(coded);
		System.out.print("  Alphabet back ?: " + clear);
		
		if (alphabet.equals(clear))
			System.out.println("  Seems to work :)");
		else
			System.out.println("  Oops!!! Bug ...");
		System.out.println();
		String freq = "eeeeeeeeeeeeeeeeaaaaaaaaaaaaaaaazzzzzzzzzzzzzzz";
		vigenere.setKey("AnotherKey");
		System.out.println("Frequency test");
		System.out.println("Frequency String: " + freq);
		
		coded = vigenere.encode(freq);
		
		System.out.println(" as codeed String: " + coded);
		System.out.println("     end back ? : " + vigenere.decode(coded));
		System.out.println();
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter the key: ");
		
		String key = scan.nextLine();
		Vigenere userVigenere = new Vigenere(key);
		
		System.out.print("Enter message: ");
		String message = scan.nextLine();
		System.out.println("     Original: " + message);
		
		coded = userVigenere.encode(message);
		System.out.println("Encoded message is: " + coded);
		System.out.println("translated back ? : " + userVigenere.decode(coded));
		scan.close();
	}
}