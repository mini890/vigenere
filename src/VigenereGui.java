import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;

public class VigenereGui extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	private Vigenere vigenere;
	
	private JTextField keyText;
	private JTextField clearTextIn;
	private JLabel encodedTextOut;
	private JTextField codedTextIn;
	private JLabel decodedTextOut;
	
	VigenereGui() {
		super ("Vigenere encoding/decoding by Mini890");
		setLayout(new BorderLayout());
		
		JPanel textPanel = new JPanel(new GridLayout(15, 1));
		
		textPanel.add(new JLabel());
		textPanel.add(createCenteredLabel("Enter the Key here:"));
		textPanel.add(createCenteredLabel("(without a key no translation would happen)"));
		keyText = new JTextField(50);
		keyText.getDocument().addDocumentListener(new KeyListener());
		textPanel.add(keyText);
		textPanel.add(new JLabel());
		
		textPanel.add(createCenteredLabel("Enter text to encode here"));
		clearTextIn = new JTextField(50);
		clearTextIn.getDocument().addDocumentListener(new ClearListener());;
		textPanel.add(clearTextIn);
		textPanel.add(createCenteredLabel("Coded Text"));
		encodedTextOut = createOutputLabel();
		textPanel.add(encodedTextOut);
		
		textPanel.add(new JLabel());
		textPanel.add(createCenteredLabel("Enter text to decode here"));
		codedTextIn = new JTextField(50);
		codedTextIn.getDocument().addDocumentListener(new CodedListener());
		textPanel.add(codedTextIn);
		textPanel.add(createCenteredLabel("Decoded Text"));
		decodedTextOut = createOutputLabel();
		textPanel.add(decodedTextOut);
		textPanel.add(new JLabel());
		
		add(textPanel, BorderLayout.CENTER);
		
		vigenere = new Vigenere("");
		
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocation(30, 30);
		pack();
		setVisible(true);
	}
	
	private JLabel createCenteredLabel(String text) {
		JLabel label = new JLabel(text);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setForeground(Color.BLUE);
		return label;
	}
	
	private JLabel createOutputLabel() {
		JLabel label = new JLabel();
		label.setOpaque(true);
		label.setBackground(Color.WHITE);
		label.setBorder(BorderFactory.createLineBorder(Color.RED));
		return label;
	}
	
	private void updateKeyString() {
		vigenere.setKey(keyText.getText());
		updateCodedString();
		updateDecodedString();
	}
	
	private void updateCodedString() {
		String line = clearTextIn.getText();
		encodedTextOut.setText(vigenere.encode(line));
	}
	
	private void updateDecodedString() {
		String line = codedTextIn.getText();
		decodedTextOut.setText(vigenere.decode(line));
	}
	
	public static void main(String[] args) {
		new VigenereGui();
	}
	
	class CodedListener implements DocumentListener {
		
		@Override
		public void changedUpdate(DocumentEvent arg0) {
			updateDecodedString();
		}
		
		@Override
		public void insertUpdate(DocumentEvent arg0) {
			updateDecodedString();
		}
		
		@Override
		public void removeUpdate(DocumentEvent arg0) {
			updateDecodedString();
		}
	
	}
	
	class ClearListener implements DocumentListener {
		
		@Override
		public void changedUpdate(DocumentEvent arg0) {
			updateCodedString();
		}
		
		@Override
		public void insertUpdate(DocumentEvent arg0) {
			updateCodedString();
		}
		
		@Override
		public void removeUpdate(DocumentEvent arg0) {
			updateCodedString();
		}
	}
	
	class KeyListener implements DocumentListener {
		
		@Override
		public void changedUpdate(DocumentEvent arg0) {
			updateKeyString();
		}
		
		@Override
		public void insertUpdate(DocumentEvent arg0) {
			updateKeyString();
		}
		
		@Override
		public void removeUpdate(DocumentEvent arg0) {
			updateKeyString();
		}
		
	}

}